# -*- coding: utf-8 -*-
import os
import sys


def setWorkingDirs(dataDir='rawData', outDir=None):
    cwDir = os.getcwd()
    if dataDir is None:
        dataDir = cwDir + os.sep
    else:
        if not os.path.exists(dataDir):
            print('the input directory', dataDir, 'does not exists. Fix it.')
            sys.exit()
        else:
            dataDir = os.path.abspath(dataDir)
            if dataDir[-1] != os.sep:
                dataDir += os.sep
    if outDir is None:
        outDir = cwDir
    else:
        outDir = os.path.abspath(outDir) + os.sep
    outputDir = pathJoinCheck('output', outDir)
    mapsDir = pathJoinCheck('maps', outDir)
    sequencesDir = pathJoinCheck('sequences', outDir)
    return (dataDir, outDir, outputDir, sequencesDir, mapsDir)


def pathJoinCheck(dir2add, rootPath='.'):
    """Check the existence of a path and build it if necessary."""
    path = os.path.join(rootPath, dir2add)
    if not os.path.exists(path):
        os.makedirs(path)
    return path


def pathFilename(path, filename):
    return os.path.join(path, filename)
